### A Pluto.jl notebook ###
# v0.15.1

using Markdown
using InteractiveUtils

# ╔═╡ cdc326cf-fd17-44b1-90df-3b16597e255a
begin
	# Activate the environment
	using Pkg
	Pkg.activate(".")

	# Load the packages
	using DataFrames
	using PlutoUI

	# Load our custom functions
	include("load_data.jl")
	include("Fitting.jl")
end

# ╔═╡ 501f74eb-4c0b-4466-b0be-fc4aafc4b6d9
md"## Activate the environment & load the packages"

# ╔═╡ 9fa3b7e1-6e21-4a3d-a04c-bc876de8b3b6
md"## Load the data"

# ╔═╡ c45e1b76-99a0-4665-a1df-1357d4a25c0b
data = LoadData();

# ╔═╡ 70389a16-360b-467b-b33e-53370e07608f
describe(data)

# ╔═╡ 26e8fcc8-a9a8-4bd2-a1ca-cdd9d4d3d7a5
md"## Model fitting"

# ╔═╡ 37f401b5-cabe-439b-9bcd-47ff7ee98268
begin
	Θ = zeros(2)
	with_terminal() do
		Θ = fitting(
			lower = [0.1, 0.1],
			upper = [1, 20],
			AllowedInitialVals = [0.2:0.1:0.9, 2:1:19],
			ModelResponses=data.Response,
			ModelRewards=data.Rewards,
			nIter = 5,
		)
	end
end

# ╔═╡ 32d12aed-6525-4269-a6d4-59d13f392dc3
begin
	α, β = Θ
	Print("Best parameters fit:\nα = $(α)\nβ = $(β)")
end

# ╔═╡ 1cb28891-17ec-4be2-9731-f7fa9c6bafd0
md"## Model simulation & accuracy score"

# ╔═╡ dd5d612b-ed85-4eb6-a275-3300089d5011
p = model(Θ, data.Response, data.Rewards)

# ╔═╡ 4810bfac-a4e0-4db9-a5c7-d6361fdf0e29
begin
	acc = accuracy(predict(p), data.Correct_response)
	Print("Model accuracy: $(round(acc*100, digits=3))%")
end

# ╔═╡ Cell order:
# ╟─501f74eb-4c0b-4466-b0be-fc4aafc4b6d9
# ╠═cdc326cf-fd17-44b1-90df-3b16597e255a
# ╟─9fa3b7e1-6e21-4a3d-a04c-bc876de8b3b6
# ╠═c45e1b76-99a0-4665-a1df-1357d4a25c0b
# ╠═70389a16-360b-467b-b33e-53370e07608f
# ╟─26e8fcc8-a9a8-4bd2-a1ca-cdd9d4d3d7a5
# ╠═37f401b5-cabe-439b-9bcd-47ff7ee98268
# ╠═32d12aed-6525-4269-a6d4-59d13f392dc3
# ╟─1cb28891-17ec-4be2-9731-f7fa9c6bafd0
# ╠═dd5d612b-ed85-4eb6-a275-3300089d5011
# ╠═4810bfac-a4e0-4db9-a5c7-d6361fdf0e29

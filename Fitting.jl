using ProgressBars
using Optim
include("Likelihood.jl")

function fitting(;
    lower,
    upper,
    AllowedInitialVals,
    ModelResponses,
    ModelRewards,
    nIter = 10,
)

    inner_optimizer = GradientDescent()

    likelihoods = zeros(nIter)
    Θ = zeros(nIter, length(lower))
    for iter in ProgressBar(1:nIter)

        initial_x = [rand(i) for i in AllowedInitialVals]
        printstyled("Initial values: $(initial_x)\n"; color = :blue)
        res = optimize(
            x -> Likelihood(x, ModelResponses, ModelRewards),
            lower,
            upper,
            initial_x,
            Fminbox(inner_optimizer),
            Optim.Options(iterations = 10, show_trace = true),
        )
        likelihoods[iter] = Optim.minimum(res)
        Θ[iter, :] = Optim.minimizer(res)

    end

    best_fit = Θ[argmin(likelihoods), :]
end

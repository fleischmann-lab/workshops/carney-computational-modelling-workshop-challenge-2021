using Statistics

function Likelihood(Θ, Responses, Rewards)

    α = Θ[1]
    β = Θ[2]

    choices = unique(Responses)
    nTrials = length(Rewards)
    V = zeros(nTrials, length(choices))
    proba = zeros(nTrials, length(choices))

    for t = 1:nTrials-1

        proba[t, :] = exp.(V[t, :] * β) / sum(exp.(V[t, :] * β))

        δ = Rewards[t] - V[t, Responses[t]] # Reward prediction error
        V[t+1, :] = V[t, :]
        V[t+1, Responses[t]] = V[t, Responses[t]] + α * δ

    end
    proba[nTrials, :] = exp.(V[nTrials, :] * β) / sum(exp.(V[nTrials, :] * β))

    logLike = log.(mean(proba, dims = 2))
    Likelihood = -sum(logLike)

end


function model(Θ, Responses, Rewards)

    α = Θ[1]
    β = Θ[2]

    choices = unique(Responses)
    nTrials = length(Rewards)
    V = zeros(nTrials, length(choices))
    proba = zeros(nTrials, length(choices))

    for t = 1:nTrials-1

        proba[t, :] = exp.(V[t, :] * β) / sum(exp.(V[t, :] * β))

        δ = Rewards[t] - V[t, Responses[t]] # Reward prediction error
        V[t+1, :] = V[t, :]
        V[t+1, Responses[t]] = V[t, Responses[t]] + α * δ

    end
    proba[nTrials, :] = exp.(V[nTrials, :] * β) / sum(exp.(V[nTrials, :] * β))

    return proba
end


function predict(proba)
    res = [argmax(proba[row, :]) for row = 1:size(proba)[1]]
end

function accuracy(predicted, groundtruth)
    accuracy = sum(predicted .== groundtruth) / length(groundtruth)
end

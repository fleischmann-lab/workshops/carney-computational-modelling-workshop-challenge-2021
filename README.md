# Carney Computational Modeling Workshop Challenge 2021


## What specific data did you model?
The data used for this project is the `traindata2.csv` file from the RLWM task, specifically the `Response` and `Rewards` of the participants.

## What was your hypothesis?
The hypothesis used here is that the participants learning is purely based on reinforcement learning.

## Model mechanism 
The model implements the delta rule (Rescorla & Wagner, 1972), which updates its learned values at each trial based on the reward prediction error. The model takes as input the `Response` and `Rewards` of the participants and outputs the choice probabilities it has learned. 

## Number and description of parameters
- Θ: vector of the model parameters
  - α: learning rate of the reinforcement learning model
  - β: softmax inverse temperature parameter

## Description of the fitting procedure
The algorithm used is a [Gradient Descent](https://julianlsolvers.github.io/Optim.jl/stable/#algo/gradientdescent/) with a maximum of 10 iterations of the gradient. The algorithm is run 5 times with different initial values of Θ to avoid getting stuck in a local minimum and then we select the best fit of these 10 iterations. The code of the fitting procedure is available in the [`Fitting.jl`](https://gitlab.com/fleischmann-lab/workshops/carney-computational-modelling-workshop-challenge-2021/-/blob/main/Fitting.jl) file and the computation of the likelihood is available in the [`Likelihood.jl`](https://gitlab.com/fleischmann-lab/workshops/carney-computational-modelling-workshop-challenge-2021/-/blob/main/Likelihood.jl) file.

## Model analysis
The model analysis can be found in the following [static notebook](https://fleischmann-lab.gitlab.io/workshops/carney-computational-modelling-workshop-challenge-2021/Analysis.html).


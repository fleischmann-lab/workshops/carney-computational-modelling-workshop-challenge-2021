using CSV
using DataFrames

function LoadData()
    data_path = "./RLWM_Data/traindata2.csv"
    data = CSV.File(data_path, header = false) |> DataFrame
    rename!(
        data,
        [
            "Participant_ID",
            "Session",
            "Block_number",
            "Correct_Incorrect",
            "Set_size",
            "RT",
            "Stimulus_ID",
            "Stimulus_ID_2",
            "Rewards",
            "Correct_response",
            "Response",
            "Global_stimulus_ID",
        ],
    )
    return data
end

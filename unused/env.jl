using CSV
using DataFrames
using StatsBase

data_path = "../Modelling Challenge/RLWM_Data/traindata2.csv"
df = CSV.File(data_path, header = false) |> DataFrame
rename!(
    df,
    [
        "Participant_ID",
        "Session",
        "Block_number",
        "Correct_Incorrect",
        "Set_size",
        "RT",
        "Stimulus_ID",
        "Stimulus_ID_2",
        "Rewards",
        "Correct_response",
        "Response",
        "Global_stimulus_ID",
    ],
)

function env()
    row = sample(1:size(df)[1], 1)
    stimulus, reward, reponse = df[row[1], [:Stimulus_ID, :Rewards, :Correct_response]]
end
